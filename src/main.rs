extern crate clap;
extern crate stocklib;

use clap::{Arg, App};
use stocklib::lists::dax::Dax;
use stocklib::lists::techdax::Techdax;
use stocklib::lists::stoxx50e::Stoxx50e;
use stocklib::lists::mdax::Mdax;

fn main() {
    let matches = App::new("Stock Simulator")
        .version("0.1.0")
        .author("Sickeler K. <k.sickeler@gmail.com>")
        .about("Simulate stock market")
        .arg(Arg::with_name("print")
             .short("p")
             .long("print")
             .value_name("listname")
             .help("Print the list indizes of given market")
             .takes_value(true))
        .arg(Arg::with_name("all")
             .short("a")
             .long("all")
             .help("Print the list indizes of given market")
             .takes_value(false))
        .get_matches();

    if matches.is_present("all") {
        all();
    }

    if let Some(n) = matches.value_of("print") {
        match n {
            "Dax" => println!("{:?}", Dax::new()),
            "mdax" => println!("{:?}", Mdax::new()),
            "techdax" => println!("{:?}", Techdax::new()),
            "Stoxx50e" => println!("{:?}", Stoxx50e::new()),
            "stoxx" => println!("{:?}", Stoxx50e::new()),
            _ => all(),
        }
    }
}

fn all() {
    println!("{:?}", Dax::new());
    println!("{:?}", Mdax::new());
    println!("{:?}", Techdax::new());
    println!("{:?}", Stoxx50e::new());
}
